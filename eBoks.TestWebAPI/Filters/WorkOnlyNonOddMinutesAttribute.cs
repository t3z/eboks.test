﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace eBoks.TestWebAPI.Filters
{
    public class WorkOnlyNonOddMinutesAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            var ctx = actionContext;
            if ((DateTime.UtcNow.Minute%2) != 0)
            {
                ctx.Response = ctx.Request.CreateErrorResponse(HttpStatusCode.BadRequest,
                    "This service works only on odd minutes");
            }
            else
            {
                base.OnActionExecuting(ctx);
            }
        }
    }
}