﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Http;
using eBoks.TestWebAPI.Filters;

namespace eBoks.TestWebAPI.Controllers
{
    [RoutePrefix("multiplier")]
    public class MultiplierController : ApiController
    {
        private static int MAX_VALUE = int.MaxValue/2;
        private static int MIN_VALUE = int.MinValue/2;

        [HttpGet, Route("")]
        [WorkOnlyNonOddMinutes]
        public int DoubleNumber(int value)
        {
            var rangeValidator = new RangeAttribute(MIN_VALUE, MAX_VALUE);
            if (rangeValidator.IsValid(value))
            {
                return value*2;
            }
            var error = rangeValidator.FormatErrorMessage("value");
            throw new ArgumentException(error);
        }
    }
}
