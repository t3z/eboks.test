﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Web;

namespace eBoks.TestApp
{
    class Program
    {
        static void Main()
        {
            var configurationErrors = new List<string>();
            var uri = Configuration.ServiceUri;
            if (!uri.IsValidUrl())
            {
                var message = string.Format("\'{0}\' is not a valid service URL", uri);
                configurationErrors.Add(message);
            }

            var resource = Configuration.ServiceResourceName;
            if (resource == null || !Uri.TryCreate(uri, resource, out uri))
            {
                configurationErrors.Add("Service resource name is invalid");
            }

            var parameterName = Configuration.ServiceMethodParam;
            if (string.IsNullOrEmpty(parameterName))
            {
                configurationErrors.Add("Service parameter name cannot be empty");
            }

            if (configurationErrors.Any())
            {
                foreach (var error in configurationErrors)
                {
                    Console.WriteLine(error);
                }
                return;
            }

            Run(uri, parameterName);
        }

        private static void Run(Uri uri, string parameterName)
        {
            var rnd = new Random(DateTime.UtcNow.Millisecond);
            while (true)
            {
                var webClient = new WebClient();
                var builder = new UriBuilder(uri);
                var query = HttpUtility.ParseQueryString(builder.Query);
                query[parameterName] = rnd.Next(0, 100 + 1).ToString();
                builder.Query = query.ToString();
                var requestUri = builder.ToString();
                try
                {
                    Log("Calling " + requestUri);
                    var result = webClient.DownloadString(requestUri);
                    Log(result);
                }
                catch (WebException wex)
                {
                    var responseStream = wex.Response.GetResponseStream();
                    if (responseStream != null)
                    {
                        var message = new StreamReader(responseStream).ReadToEnd();
                        Log(string.Format("Error message: {0}", message));
                    }
                    else
                    {
                        Log(wex.ToString());
                    }
                }
                catch (Exception ex)
                {
                    Log(ex.ToString());
                }
                finally
                {
                    webClient.Dispose();
                    var waitTime = rnd.Next(1, Configuration.WaiTime + 1);
                    Thread.Sleep(TimeSpan.FromSeconds(waitTime));
                }
            }
        }

        private static void Log(string message)
        {
            Console.WriteLine("{0}: {1}", DateTime.Now.ToString("s"), message);
        }
    }
}
