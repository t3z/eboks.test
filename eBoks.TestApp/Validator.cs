﻿using System;

namespace eBoks.TestApp
{
    internal static class Validator
    {
        public static bool IsValidUrl(this Uri uri)
        {
            return uri != null && (uri.Scheme == Uri.UriSchemeHttp || uri.Scheme != Uri.UriSchemeHttps) && string.IsNullOrEmpty(uri.Query);
        }
    }
}