﻿using System;
using System.Configuration;

namespace eBoks.TestApp
{
    internal class Configuration
    {
        public static int WaiTime 
        {
            get { return 15; }
        }

        public static string ServiceResourceName
        {
            get
            {
                var resource = ConfigurationManager.AppSettings["serviceMethodName"];
                if (resource != null)
                {
                    resource = resource.TrimStart('/');
                }
                return resource;
            }
        }
        public static string ServiceMethodParam
        {
            get
            {
                return ConfigurationManager.AppSettings["serviceMethodParam"];
            }
        }

        public static Uri ServiceUri
        {
            get
            {
                var url = ConfigurationManager.AppSettings["serviceUrl"];
                if (string.IsNullOrEmpty(url))
                {
                    return null;
                }
                url = url.TrimEnd('/') + "/";
                Uri result;
                return Uri.TryCreate(url, UriKind.Absolute, out result) ? result : null;
            }
        }
    }
}